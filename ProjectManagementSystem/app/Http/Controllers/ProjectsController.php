<?php

namespace App\Http\Controllers;

use App\models\Task;
use Illuminate\Http\Request;
use App\models\Projects;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Projects::all(); //get all projects

        return view('projects.index', ['projects' => $projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Projects::all();
        return view('projects.create', ['projects'=>$projects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name and project_id field
        $this->validate($request, [
                'name'=>'required|max:150',
                'project_id' =>'required',
            ]
        );

        $project = new Projects();
        $project->name = $request['name'];
        $project->clients_id = $request['clients_id'];
        $project->description =  $request['description'];

        $project->save();

        flash('project '. $project->name.' was added!');
        return redirect()->route('projects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('projects');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $task = Task::findOrFail($id);
//        $this->validate($request, [
//                'name'=>'required|max:150',
//                'project_id' =>'required',
//            ]
//        );
//
//        // $task->fill($request)->save();
//
//        $task->name = $request['name'];
//        $task->project_id = $request['project_id'];
//        $task->description =  $request['description'];
//        $task->start =  $request['start'];
//        $task->end = $request['end'];
//        $task->status = $request['status'];
//
//        $task->save();
//
//        flash('Task '. $task->name.' was updated!');
//        return redirect()->route('tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $task = Task::findOrFail($id);
//        $task->delete();
//
//        flash('Task ' . $task->name . ' was deleted!');
//        return redirect()->route('tasks.index');
    }
}
