<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Task;
use App\models\Projects;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all(); //get all tasks
        $projects=Projects::all();
        return view('tasks.index', ['tasks' => $tasks,'projects'=>$projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Projects::all();
        $statuses = ['new' => 'new',
            'in_progress' => 'in_progress',
            'completed' => 'completed',
            'canceled' => 'canceled'];
        return view('tasks.create', ['projects_name'=>$projects, 'statuses' => $statuses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name and project_id field
        $this->validate($request, [
                'name'=>'required|max:150',
                'project_id' =>'required',
            ]
        );
        $task = new Task();
        $task->name = $request['name'];
        $task->project_id = $request['project_id'];
        $task->description =  $request['description'];
        $task->start =  $request['start'];
        $task->end = $request['end'];
        $task->status = $request['status'];

        $task->save();

        flash('Task '. $task->name.' was added!');
        return redirect()->route('tasks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('tasks');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $projects = Projects::all();
        $statuses = ['new' => 'new',
            'in_progress' => 'in_progress',
            'completed' => 'completed',
            'canceled' => 'canceled'];
        return view('tasks.edit', compact('task', 'projects', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $this->validate($request, [
                'name'=>'required|max:150',
                'project_id' =>'required',
            ]
        );

       // $task->fill($request)->save();

        $task->name = $request['name'];
        $task->project_id = $request['project_id'];
        $task->description =  $request['description'];
        $task->start =  $request['start'];
        $task->end = $request['end'];
        $task->status = $request['status'];

        $task->save();

        flash('Task '. $task->name.' was updated!');
        return redirect()->route('tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        flash('Task ' . $task->name . ' was deleted!');
        return redirect()->route('tasks.index');
    }
}
