<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Technology extends Model
{
    /**
     * Table, connected with model.
     *
     * @var string
     */
    protected $table = 'technologies';
    /**
     * Defines if timestamps updated_at and created_at have to be processed.
     *
     * @var bool
     */
    protected $fillable = [
        'name',
        'description'
    ];

}