<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * Table, connected with model.
     *
     * @var string
     */
    protected $table = 'tasks';
    /**
     * Defines if timestamps updated_at and created_at have to be processed.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'project_id',
        'start',
        'end',
        'status'
    ];

}
