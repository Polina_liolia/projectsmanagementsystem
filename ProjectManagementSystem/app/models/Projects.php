<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    /**
     * Table, connected with model.
     *
     * @var string
     */
    protected $table = 'projects';
    /**
     * Defines if timestamps updated_at and created_at have to be processed.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'clients_id'
    ];
}
