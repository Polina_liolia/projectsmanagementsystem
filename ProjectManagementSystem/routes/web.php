<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');
Route::resource('tasks', 'TaskController');
Route::resource('technologies', 'TechnologyController');
Route::resource('projects', 'ProjectsController');

Route::middleware(["rolespermissionsverifier:roles:Admin;User|permissions:adminperm"])->group(function (){
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('technologies', 'TechnologyController');
    Route::resource('projects', 'ProjectsController');
});

Route::middleware(["rolespermissionsverifier:roles:Admin;User|permissions:EditProfile"])->group(function (){
    Route::resource('users', 'UserController', ['only' => [
        'edit', 'update'
    ]]);
});

Route::middleware(["rolespermissionsverifier:roles:Admin;User|permissions:ClientSee"])->group(function (){
    Route::resource('users', 'UserController', ['only' => [
        'index'
    ]]);
});

Route::middleware(["rolespermissionsverifier:roles:Admin;User|permissions:attachTechnologies"])->group(function (){
    Route::resource('technologies', 'TechnologyController', ['only' => [
        'index', 'create', 'edit', 'update'
    ]]);
});

Route::middleware(["rolespermissionsverifier:roles:ProjectMan;"])->group(function (){
    Route::resource('users', 'UserController', ['only' => [
        'showEmployees', 'showClients'
    ]]);
});

Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
# Проверяем, что у пользователя выбран доступный язык
        Session::put('locale', $locale);
# И устанавливаем его в сессии под именем locale
    }
    return redirect()->back();
# Редиректим его <s>взад</s> на ту же страницу
});

Route::put('inactive/{id}',array('uses' => 'UserController@inactive', 'as' => 'users.inactive'), function ($id){

});

Route::get('showEmployees/',array('uses' => 'UserController@showEmployees', 'as' => 'users.showEmployees'));

Route::get('showClients/',array('uses' => 'UserController@showClients', 'as' => 'users.showClients'));