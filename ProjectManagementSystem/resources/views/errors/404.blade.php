@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img class="col-md-8 col-md-offset-2" src="{{url('./img/404.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection