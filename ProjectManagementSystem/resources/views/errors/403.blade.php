@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img src="{{url('./img/403.png')}}" style="height: 480px">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
