<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('layouts.partials._head')
<body class="w3-theme-l5">
    @include('layouts.partials._navbar')
    <div id="app" class="w3-container w3-content" style="max-width:2560px;max-height:1440px;margin-bottom:142px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @include('flash::message')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @guest
            @yield('content')
            @else
        <div class="container">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="w3-row">
                <!-- Left Column -->
                <div class="w3-col m3">
                    <!-- Profile -->
                    <div class="w3-card w3-round w3-white">
                        <div class="w3-container">
                            <h4 class="w3-center">{{__('My Profile')}}</h4>
                            @if (Auth::user()->hasRole('Admin'))
                                <p class="w3-center"><img src="{{url('./img/avatars/admin.jpg')}}" class="w3-circle" style="height:106px;width:106px" alt="Avatar" id="Avatar"></p>
                            @elseif (Auth::user()->hasRole('ProjectMan'))
                                <p class="w3-center"><img src="{{url('./img/avatars/pm.jpg')}}" class="w3-circle" style="height:106px;width:106px" alt="Avatar" id="Avatar"></p>
                            @elseif (Auth::user()->hasRole('TeamLeader'))
                                <p class="w3-center"><img src="{{url('./img/avatars/tl.jpg')}}" class="w3-circle" style="height:106px;width:106px" alt="Avatar" id="Avatar"></p>
                            @elseif (Auth::user()->hasRole('Programmer'))
                                <p class="w3-center"><img src="{{url('./img/avatars/dev.jpg')}}" class="w3-circle" style="height:106px;width:106px" alt="Avatar" id="Avatar"></p>
                            @elseif (Auth::user()->hasRole('Client'))
                            <p class="w3-center"><img src="{{url('./img/avatars/client.jpg')}}" class="w3-circle" style="height:106px;width:106px" alt="Avatar" id="Avatar"></p>
                            @else
                                <p class="w3-center"><img src="{{url('./img/avatars/other.jpg')}}" class="w3-circle" style="height:106px;width:106px" alt="Avatar" id="Avatar"></p>
                            @endif
                            <button class="w3-button w3-block w3-theme-l4" onclick="location.href= '{{ route('users.edit', Auth::user()->id) }}'">{{__('Edit')}}</button>
                            <hr>
                            <p><i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme"></i>{{ Auth::user()->name }}</p>
                            <p><i class="fa fa-home fa-fw w3-margin-right w3-text-theme"></i>{{ Auth::user()->email }}</p>
                            <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i>{{ Auth::user()->created_at->format('F d, Y h:ia') }}</p>
                        </div>
                    </div>
                    <!-- End Profile -->
                    <br>
                    <!-- Accordion -->
                    <div class="w3-card w3-round">
                        <div class="w3-white">
                            @if(Auth::user()->hasRole('Admin'))
                                @ability('Admin', 'adminperm')
                                <button onclick="location.href='{{ route('roles.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-calendar-check-o fa-fw w3-margin-right"></i> {{__('Roles')}}</button>
                                <button onclick="location.href='{{ route('permissions.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Permissions')}}</button>
                                <button onclick="location.href='{{ route('users.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-users fa-fw w3-margin-right"></i> {{__('Users')}}</button>
                                <button onclick="location.href='{{ route('users.showEmployees') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right" id="employees"></i> {{__('Employees')}}</button>
                                <button onclick="location.href='{{ route('users.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Programmers')}}</button>
                                <button onclick="location.href='{{ route('users.showClients') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Clients')}}</button>
                                <button onclick="location.href='{{ route('projects.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Projects')}}</button>
                                <button onclick="location.href='{{ route('tasks.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Tasks')}}</button>
                                <button onclick="location.href='{{ route('technologies.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Technologies')}}</button>
                                @endability
                            @elseif (Auth::user()->hasRole('ProjectMan'))
                                <button onclick="location.href='{{ route('users.showClients') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Clients')}}</button>
                                <button onclick="location.href='{{ route('projects.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Projects')}}</button>
                                <button onclick="location.href='{{ route('tasks.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Tasks')}}</button>
                                <button onclick="location.href='{{ route('users.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Programmers')}}</button>
                                <button onclick="location.href='{{ route('technologies.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Technologies')}}</button>
                                <button onclick="location.href='{{ route('users.showEmployees') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right" id="employees"></i> {{__('Employees')}}</button>
                            @elseif (Auth::user()->hasRole('TeamLeader'))
                                <button onclick="location.href='{{ route('projects.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Projects')}}</button>
                                <button onclick="location.href='{{ route('tasks.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Tasks')}}</button>
                                <button onclick="location.href='{{ route('users.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Programmers')}}</button>
                                <button onclick="location.href='{{ route('technologies.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                    <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Technologies')}}</button>
                            @elseif (Auth::user()->hasRole('Client'))
                            <button onclick="location.href='{{ route('projects.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Projects')}}</button>
                            <button onclick="location.href='{{ route('tasks.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Tasks')}}</button>
                            @elseif (Auth::user()->hasRole('Programmer'))
                            <button onclick="location.href='{{ route('projects.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Projects')}}</button>
                            <button onclick="location.href='{{ route('tasks.index') }}'" class="w3-button w3-block w3-theme-l1 w3-left-align">
                                <i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> {{__('Tasks')}}</button>
                            @endif
                        </div>
                    </div>
                    <!-- End Accordion -->
                    <br>
                </div>
                <!-- End Left Column -->
                <!-- Middle Column -->
                <div class="w3-col m7">
                    <div class="w3-row-padding">
                        <div class="w3-col m12">
                            <div class="w3-card w3-round w3-white">
                                <div class="w3-container w3-padding">
                                    <p>@yield('content')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w3-container w3-card w3-white w3-round w3-margin w3-padding" id="content"><br>
                        <span class="w3-right w3-opacity">16 min</span>
                        <h4>Jane Doe</h4><br>
                        <hr class="w3-clear">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <button type="button" class="w3-button w3-theme-d1 w3-margin-bottom"><i class="fa fa-thumbs-up"></i>  Like</button>
                        <button type="button" class="w3-button w3-theme-d2 w3-margin-bottom"><i class="fa fa-comment"></i>  Comment</button>
                    </div>
                </div>
                <!-- End Middle Column -->
                <!-- Right Column -->
                <div class="w3-col m2">
                    <div class="w3-card w3-round w3-white w3-center w3-padding">
                        <div class="w3-container">
                            <p>Upcoming Events:</p>
                            <img src="{{url('./img/forest.jpg')}}" alt="Forest" style="width:100%;">
                            <p><strong>Holiday</strong></p>
                            <p>Friday 15:00</p>
                            <p><button class="w3-button w3-block w3-theme-l4" onclick="">{{__('Edit')}}</button></p>
                        </div>
                    </div>
                    <br>
                    <div class="w3-card w3-round w3-white w3-center w3-padding">
                        <div class="w3-container">
                            <p>Friend Request</p>
                            <img src="{{url('./img/222.jpg')}}" alt="Avatar" style="width:50%"><br>
                            <span>Jane Doe</span>
                            <div class="w3-row w3-opacity">
                                <div class="w3-half">
                                    <button class="w3-button w3-block w3-green w3-section" title="Accept"><i class="fa fa-check"></i></button>
                                </div>
                                <div class="w3-half">
                                    <button class="w3-button w3-block w3-red w3-section" title="Decline"><i class="fa fa-remove"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="w3-card w3-round w3-white w3-padding-16 w3-center w3-padding">
                        <p>ADS</p>
                    </div>
                    <br>
                    <div class="w3-card w3-round w3-white w3-padding-32 w3-center w3-padding">
                        <p><i class="fa fa-bug w3-xxlarge"></i></p>
                    </div>
                </div>
                <!-- End Right Column -->
            </div>
        </div>
                @endguest
    </div>
    @include('layouts.partials._footer')
    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
</body>
</html>
