@extends('layouts.app')

@section('title', '| Add Task')

@section('content')

    <div class='col-lg-4 col-lg-offset-4'>

        <h1><i class='fa fa-key'></i> {{__('Add Task')}}</h1>
        <hr>

        {{ Form::open(array('url' => 'tasks')) }}

        <div class="form-group">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
        </div><div class="form-group">
            {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description', null, array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('start', 'Starts') }}
            {{ Form::date('start', \Carbon\Carbon::now()) }}
        </div>
        <div class="form-group">
            {{ Form::label('end', 'Ends') }}
            {{ Form::date('end', \Carbon\Carbon::now()) }}
        </div>

        <div class="form-group">
            {{ Form::label('project_id', 'Project') }}
            {{ Form::select('project_id',  $projects_name->pluck('name'))}}
        </div>

        <div class="form-group">
            {{ Form::label('status', 'Status') }}
            {{ Form::select('status', $statuses)}}
        </div>

        {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>

@endsection