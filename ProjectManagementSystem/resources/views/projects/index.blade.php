@extends('layouts.app')

@section('title', '| Projects')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        <h1><i class="fa fa-key"></i> {{__('Projects')}}
            {{--            <a href="{{ route('projects.index') }}" class="btn btn-default pull-right">Projects</a>--}}
        </h1>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{__('Project Name')}}</th>
                    <th>{{__('Description')}}</th>
                    <th>{{__('Client Id')}}</th>
                    <th>{{__('Operations')}}</th>

                </tr>
                </thead>
                <tbody>
                @foreach ($projects as $project)
                    <tr>
                        <td>{{ $project->name }}</td>
                        <td>{{ $project->description }}</td>
                        <td>{{ $project->clients_id }}</td>
                        <td>
                            <a href="{{ URL::to('projects/'.$project->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                            {!! Form::open(['method' => 'DELETE', 'route' => ['projects.destroy', $project->id] ]) !!}
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger')) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    {{--@endif--}}
                @endforeach
                </tbody>
            </table>
        </div>

        <a href="{{ URL::to('projects/create') }}" class="btn btn-success">{{__('Add Project')}}</a>

    </div>
@endsection
