@extends('layouts.app')

@section('title', '| Add Project')

@section('content')

    <div class='col-lg-4 col-lg-offset-4'>

        <h1><i class='fa fa-key'></i> {{__('Add Project')}}</h1>
        <hr>

        {{ Form::open(array('url' => 'projects')) }}

        <div class="form-group">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
        </div><div class="form-group">
            {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description', null, array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            {{ Form::label('clients_id', 'Client Id') }}
            {{ Form::select('clients_id', $projects)}}
        </div>

        {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>

@endsection